function map(items, callback){
    let newArr = [];
    for(let i=0; i<items.length; i++){
        let original = items[i];
        newArr[i] = callback(original);
    }
    console.log(newArr);   
}

module.exports = map;