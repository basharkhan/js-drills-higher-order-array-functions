const nestedArray = [1, [2], [[3]], [[[4]]]];

function flatten(nestedArray) {

    let result = [];

    for (let i = 0; i < nestedArray.length; i++) {

        if (Array.isArray(nestedArray[i])) {
            result = result.concat(flatten(nestedArray[i]));

        }
        else {
            result.push(nestedArray[i]);
        }

    }
    return result;

}

const ans = flatten(nestedArray);

module.exports = ans;