function find(items, callback) {

    for (let i = 0; i < items.length; i++) {
        let element = items[i];
        if (callback(element)) {
            console.log(element);
        }
        else {
            console.log('undefined');
        }
    }

}

module.exports = find;