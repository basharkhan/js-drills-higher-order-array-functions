function each(items, callback){
    
    let result = []; 
    let index = [];

    for(let i=0; i<items.length; i++){
        result[i] = items[i] + 1;
        index[i] = i;
    }
    callback(result, index);   
}

module.exports = each ;




