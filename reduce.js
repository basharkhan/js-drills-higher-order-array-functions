function reduce(items, callback) {

    let startingValue = items[0];
    let sum;
    for (let i = 1; i < items.length; i++) {
        currentValue = items[i];
        sum = callback(startingValue, currentValue);
        startingValue = sum;
    }
    console.log(sum);
};


module.exports = reduce;