const items = require('../items');
const reduce = require('../reduce');

reduce(items, function callback(startingValue, currentValue) {
    return startingValue + currentValue;
});