const items = require('../items');
const filter = require('../filter');

filter(items, function callback(r) {
    let result = [];
    for (let i = 0; i < r.length; i++) {
        if (r[i] % 7 === 0) {
            result.push(r[i]);
        }
    }
    r = result;
    return r;
});
